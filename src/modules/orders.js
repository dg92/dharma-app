import api from 'utils/api';
import keducer from 'utils/keducer';
import {random} from 'utils/string';

export default keducer('orders', {}, {
  sellAmount: 0,
  receiverAmount: 0,
  expiration: '',
  taker: '',
  orderHash: '',
  message: '',
  err: null,
  bitlyLink: null,
  order: {
    message: {
      text: '',
      png: ''
    }
  },
  markedDownMessage: ''
});

export function addValuesToOrder(data) {
  let newObj = {};
  Object.keys(data).map((key) => {
    newObj[key] = data[key];
  });
  newObj['orderHash'] = '0x'+random().replace(/-/g, '');
  return {type: 'orders.saveValue', payload: newObj};
}

export function checkForErrors(data) {
  if(data.sellAmount ===0 || data.receiverAmount === 0) {
    return {type: 'orders.errorCheck', payload: {err: 'Sell amount and Receiver amount cannot be 0 and message box cannot be empty.'}};
  } else {
    return {type: 'orders.errorCheck', payload: {err: null}};
  }
}


export function createOrder(props, state, img) {
  const data = {
    wallet_address: random(),
    seller_info: {
      contract_address: random(),
      token_name: state.fromToken.name,
      symbol: state.fromToken.symbol,
      amount: props.sellAmount
    },
    buyer_info: {
      contract_address: random(),
      token_name: state.toToken.name,
      symbol: state.toToken.symbol,
      amount: props.receiverAmount
    },
    expiration: {
      date: props.expiration
    },
    taker: props.taker,
    order_hash: props.orderHash,
    message: {
      text: props.markedDownMessage,
      png: img
    }
  }
  return (dispatch) => {
    api
      .post('/orders', data)
      .then(({order, bitlyLink}) => {
        dispatch({
          type: 'orders.createSuccess',
          payload: {bitlyLink}
        });
      }).catch(err => {
        dispatch({
          type: 'orders.createFailed',
          payload: {err}
        });
      })
    ;
  };
}

export function getOrderById(orderId) {
  return (dispatch) => {
    api
    .get(`/orders/${orderId}/get-details`)
      .then(({order}) => {
        dispatch({
          type: 'orders.getByIdSuccess',
          payload: {order}
        });
      }).catch(err => {
        dispatch({
          type: 'orders.getByIdFailed',
          payload: {err: err.data.err}
        });
      })
    ;
  };
}