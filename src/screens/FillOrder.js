import React from 'react';
// import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import domtoimage from 'dom-to-image';
import marked from 'marked';
import {css} from 'glamor';

import TokenSelector from 'screens/createOrder/TokenSelector';
import Input from 'components/Input';
import {getOrderById} from 'modules/orders';

class FillOrder extends React.Component {
  componentWillMount() {
    this.props.dispatch(getOrderById(this.props.match.params.orderId))
  }
  
  render() {
    return (<div className='min-vh-100 pt5 bg-lightest-gray pb5'>
      <div className='w-50 center tr b gray f6 mb4'>Fill an Order</div>
      <div className='bg-white w-60 center pv4 ph4 br3'>
        <div className='w-100 cf mt2'>
          <div className='f6 gray mb2'>Order JSON</div>
          <textarea
            className='w-100 h-100 f6 b--black-10 ba br2' value={JSON.stringify(this.props.order)}
          >
          </textarea>
        </div>

        <div className='w-100 cf mt3'>
          <div className='f6 gray mb2'>Thumbnail preview</div>
          <div id='thumbnail'>
            <div
              className='pv4 ph2 white bg-blue br2 white tc f2'
              style={{wordWrap: 'break-word'}}
              dangerouslySetInnerHTML={{__html: marked(this.props.order.message.text)}}
            />
          </div>
        </div>

        <div className='mt3'>
          <div
            className='pa2 tc dim pointer ba b--black-20 br-pill'
            onClick={() => {
              const thumbnail = document.getElementById('thumbnail');
              domtoimage
                .toPng(thumbnail)
                .then((dataUrl) => {
                  // const img = new Image();
                  // img.src = dataUrl;
                  // document.body.appendChild(img);

                  console.log('Image data-url is : ', dataUrl);
                  console.log('Now post the following data : ', this.state);
                })
                .catch((error) => {
                  console.error('oops, something went wrong!', error);
                })
              ;
            }}
          >
            Submit
          </div>
        </div>
      </div>
    </div>);
  }
}

const mapStateToProps = (state) => {
  return {
    order: state.orders.order
  }
}

export default connect(
  mapStateToProps
)(FillOrder);
