import React from 'react';
import PropTypes from 'prop-types';
import {css} from 'glamor';

import config from 'config';
import Modal from 'components/Modal';

export const getTokenImageUrl = (symbol) => `${config.frontendBase}/token-icons/${symbol.toLowerCase()}.png`;

const tokens = [
  {
    symbol: 'eos',
    name: 'EOS',
  },
  {
    symbol: 'tenx',
    name: 'TENX',
  },
  {
    symbol: 'omg',
    name: 'OmiseGO'
  },
  {
    symbol: 'ant',
    name: 'Aragon'
  },
  {
    symbol: 'salt',
    name: 'SALT'
  },
  {
    symbol: 'qtum',
    name: 'QTUM'
  }
];

class TokenSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      selectedTokenSymbol: 'eos'
    };
  }

  componentDidMount() {
    this.props.onSelect(tokens.find(t => t.symbol === this.state.selectedTokenSymbol));
  }

  render() {
    const {title, onSelect} = this.props;
    const {modalVisible, selectedTokenSymbol} = this.state;
    return (<div className='w-100 center ba b--black-10 pa2 br2 pointer hover-bg-yellow'>
      <div onClick={() => !this.state.modalVisible && this.setState({modalVisible: true})}>
        <img className= "" {...css({height: "40%", width: "40%", display: "block", marginLeft: "auto", marginRight: "auto"})} src={getTokenImageUrl(selectedTokenSymbol)} />
        <div className='pb2 tc f6'>{title} : {selectedTokenSymbol}</div>
        <Modal
          visible={modalVisible}
          onRequestClose={() => this.setState({modalVisible: false})}
        >
          <div className='f5 b mb3'>{title}</div>
          {tokens.map(t => (<div key={t.symbol}>
            <div
              className='w-30 tc center pr2 fl br2 mv3 dim'
              onClick={() => {
                this.setState({
                  selectedTokenSymbol: t.symbol,
                  modalVisible: false
                });
                onSelect(t);
              }}
            >
              <div className='b--black-10 ba dim pointer pa1 w-100'>
                <img className= "" {...css({height: "60%", width: "60%", display: "block", marginLeft: "auto", marginRight: "auto"})} src={getTokenImageUrl(t.symbol)}/>
                <div className='pb2 tc f6'>{t.name}</div>
              </div>
            </div>
          </div>))}
        </Modal>
      </div>
    </div>);
  }
}

TokenSelector.propTypes = {
  title: PropTypes.string,
  onSelect: PropTypes.func
};

export default TokenSelector;
