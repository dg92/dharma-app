import React from 'react';
// import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import domtoimage from 'dom-to-image';
import {css} from 'glamor';
import marked from 'marked';

import TokenSelector, {getTokenImageUrl} from 'screens/createOrder/TokenSelector';
import Input from 'components/Input';
import {addValuesToOrder, checkForErrors, createOrder} from 'modules/orders';
import modifiedMsg from 'utils/modifiedMsg';


class CreateOrder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  
  render() {
    const {sellAmount, receiverAmount, expiration, taker, orderHash, message, bitlyLink, markedDownMessage} = this.props;
    const {fromToken, toToken} = this.state;
    const newMessage = modifiedMsg(message, {
      MAKER_TOKEN_SYMBOL: !fromToken ? 'eos' : fromToken.symbol,
      MAKER_TOKEN_AMOUNT: sellAmount,
      TAKER_TOKEN_SYMBOL: !toToken ? 'eos' : toToken.symbol,
      TAKER_TOKEN_AMOUNT: receiverAmount
    });
    console.log({newMessage})
    return (<div className='min-vh-100 pt5 bg-lightest-gray pb5'>
      <div className='w-50 center tr b gray f6 mb4'>Generate order</div>
      <div className='bg-white w-60 center pv4 ph4 br3'>
        <div className='w-100 cf'>
          <div className='f6 gray mb4'>Choose currency pair to convert</div>
          <div className='w-40 fl-ns'>
            <TokenSelector title='From' onSelect={fromToken => this.setState({fromToken})}/>
          </div>
          <div className='w-20 fl-ns pt4 pr3'>
            <img className= ""
              {...css({height: "50%", width: "50%", display: "block", marginLeft: "auto", marginRight: "auto"})}
              src={'http://cdn.mysitemyway.com/etc-mysitemyway/icons/legacy-previews/icons/firey-orange-jelly-icons-arrows/007628-firey-orange-jelly-icon-arrows-arrow4-right.png'} />
          </div>
          <div className='w-40 fl-ns'>
            <TokenSelector title='To' onSelect={toToken => this.setState({toToken})} />
          </div>
        </div>

        <div className='w-100 cf mt3'>
          <div className='w-50 fl-ns pr3'>
            <div className='f6 black-40 mb2 mt3'>Sell Amount</div>
            <div className='w-80 fl-ns'>
              <Input value={sellAmount} placeholder="Amount" min="0" type="number" step="any" 
                onChange={e => this.props.dispatch(addValuesToOrder({sellAmount: e.target.value}))}/>
            </div>
            <div className='w-20 fl-ns bb b--black-10 pa2 f6'>
              {!fromToken ? 'EOS': fromToken.name}
            </div>
          </div>
          <div className='w-50 fl-ns pr3'>
            <div className='f6 black-40 mb2 mt3'>Receiver Amount</div>
            <div className='w-80 fl-ns'>
              <Input value={receiverAmount} placeholder="Amount" min="0" type="number" step="any" onChange={e => this.props.dispatch(addValuesToOrder({receiverAmount: e.target.value}))}/>
            </div>
            <div className='w-20 fl-ns bb b--black-10 pa2 f6'>
              {!toToken ? 'EOS' : toToken.name}
            </div>
          </div>
        </div>
        
        <div className='w-100 cf mt3'>
          <div className='f6 gray mb2'>Expiration</div>
            <input type='date' value={expiration} placeholder="Date" className='w-100 br2 pa2 f6 bb b--black-10'
              {...css({borderTop: "transparent", borderLeft: "transparent", borderRight: "transparent"})}
              onChange={e => this.props.dispatch(addValuesToOrder({expiration: e.target.value}))}/>
        </div>

        <div className='w-100 cf mt3'>
          <div className='f6 gray mb2'>Taker</div>
            <Input value={taker} placeholder="Taker" onChange={e => this.props.dispatch(addValuesToOrder({taker: e.target.value}))}/>
        </div>

        <div className='w-100 cf mt3'>
          <div className='f6 gray mb2'>Order Hash</div>
            <Input value={orderHash} placeholder="Order Hash" value={orderHash}/>
        </div>

        <div className='w-100 cf mt3'>
          <div className='f6 gray mb2'>Customise your message</div>
          <textarea
            className='w-100 f6 b--black-10 ba br2' value={message}
            onChange={e => this.props.dispatch(addValuesToOrder({
              message: e.target.value
            }))}
          >
          </textarea>
        </div>
        <div className='w-100 cf mt3'>
          <div className='f6 gray mb2'>Thumbnail preview</div>
          <div id='thumbnail'>
            <div
              className='pv4 ph2 white br2 bg-blue white tc f2'
              style={{wordWrap: 'break-word'}}
              dangerouslySetInnerHTML={{__html: marked(newMessage)}}
            />
          </div>
        </div>
        <h2>{this.props.err ? JSON.stringify(this.props.err): null}</h2>
        <div className='mt3'>
          <div
            className='pa2 tc dim pointer ba b--black-20 br-pill'
            onClick={() => {
              if(message.length > 0 && this.props.sellAmount > 0 && this.props.receiverAmount > 0) {
                this.props.dispatch(checkForErrors(this.props));
                this.props.dispatch(addValuesToOrder({markedDownMessage: newMessage}));
                const thumbnail = document.getElementById('thumbnail');
                domtoimage.toPng(thumbnail).then((dataUrl) => this.props.dispatch(createOrder(this.props, this.state, dataUrl)));
              } else {
                this.props.dispatch(checkForErrors(this.props));
              }
            }}
          >
            Submit
          </div>
          
          <h2>{bitlyLink ? `Order saved, you bitly link is: ${bitlyLink}`: null}</h2>
        </div>

      </div>
    </div>);
  }
}

const mapStateToProps = (state) => {
  return {
    sellAmount: state.orders.sellAmount,
    receiverAmount: state.orders.receiverAmount,
    expiration: state.orders.expiration,
    taker: state.orders.taker,
    orderHash: state.orders.orderHash,
    message: state.orders.message,
    err: state.orders.err,
    bitlyLink: state.orders.bitlyLink,
    markedDownMessage: state.orders.markedDownMessage
  }
}

export default connect(
  mapStateToProps
)(CreateOrder);