import React from 'react';
import {css} from 'glamor';

const Input = ({...props}) => {
  return (<input type='text' className='w-100 br2 pa2 f6 bb b--black-10'
  	{...css({borderTop: "transparent", borderLeft: "transparent", borderRight: "transparent"})}
  	{...props} />);
};

export default Input;
