import React from 'react';
import PropTypes from 'prop-types';
import {Motion, spring} from 'react-motion';

const Modal = ({visible, onRequestClose, children}) => {
  return (<div>
    {/* Overlay */}
    {visible &&
      <div
        className='fixed top-0 left-0 right-0 bottom-0 bg-black'
        style={{zIndex: 9, opacity: 0.2}}
        onClick={visible ? onRequestClose : () => {}}
      />
    }

    <Motion
      defaultStyle={{bottom: -240, opacity: 0}}
      style={{bottom: visible ? spring(80) : spring(-240), opacity: visible ? spring(1) : spring(0)}}
    >
      {style => (<div
        style={{
          ...style, zIndex: 999999, top: '50%', left: '50%',
          height: '60%',
          transform: 'translate(-50%, -50%)',
          display: style.opacity > 0 ? 'block' : 'none'
        }}
        className='fixed w-50 center shadow-2 br3'
      >
        <div className='pa4 br3 bg-white h-100'>
          {children}
        </div>
      </div>)}
    </Motion>
  </div>);
};

Modal.propTypes = {
  visible: PropTypes.bool,
  onRequestClose: PropTypes.func
};

Modal.defaultProps = {
  visible: false,
  onRequestClose: () => {}
};

export default Modal;
