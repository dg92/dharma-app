import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter, Route} from 'react-router-dom';
import 'utils/tachyons.min.css';

import 'index.css';
import store from 'store';

import CreateOrder from 'screens/CreateOrder';
import FillOrder from 'screens/FillOrder';

const App = () => (
  <BrowserRouter>
    <Provider store={store}>
      <div className='black sans-serif'>
        <Route exact path='/' component={CreateOrder} />
        <Route path='/orders/:orderId' component={FillOrder} />
      </div>
    </Provider>
  </BrowserRouter>
);

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
