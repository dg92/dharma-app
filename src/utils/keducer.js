/*
   Phonetic : "Kay-Deu-Cer"
   Allow for a map like declaration of reducers.
   Copies entire payload if no action definition is present, or run the definition if it exists.

   ex: 
   export default keducer('users', {
   'users.signInSuccess': (state, {user, token}) => ({...state, user, token, loading: false});
   })

   export function signIn(username, password) {
   ...
   dispatch({type: 'users.signInSuccess', payload: {user: res.user, token: res.token}});
   ...
   }

   //--------------------------------------------------------------------------//
   // This will work even without registering the action : 'users.loadSuccess'. 
   // Welcome for the tons of time saved.
   //-------------------------------------------------------------------------//
   export function loadUserFromCache() {
   ...
   Promise(...)
   .then((user, token) => {
   dispatch({'type': 'users.loadSuccess', payload: {user, token}});   
   })
   ; 
   ...
   }

   //--------------------------------------------------------------------------//
   // Creating default state.
   // Sometimes you want some variables to be initialised by default. For example, 
   // You can want a posts reducer to have a collection of type array by default.
   // You can achieve this by passing a 3rd argument, which will be the default
   // state of the reducer.
   //--------------------------------------------------------------------------//
 */

export default function keducer(prefix, actionMutationMap={}, defaultState={}) {
  return (state=defaultState, action) => {
    return actionMutationMap[action.type] ?
      actionMutationMap[action.type](state, action.payload ? action.payload : {}) :
      action.type.indexOf(`${prefix}.`) === 0 ? {...state, ...(action.payload)} : state
    ;
  };
}
