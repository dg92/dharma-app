
export default function modifiedMsg(message, data) {
  const modifiedMsg = message
    .replace(/<<MAKER_TOKEN_SYMBOL>>/g, data.MAKER_TOKEN_SYMBOL)
    .replace(/<<MAKER_TOKEN_LOGO>>/g, '')
    .replace(/<<MAKER_TOKEN_AMOUNT>>/g, data.MAKER_TOKEN_AMOUNT)
    .replace(/<<TAKER_TOKEN_SYMBOL>>/g, data.TAKER_TOKEN_SYMBOL)
    .replace(/<<TAKER_TOKEN_LOGO>>/g, '')
    .replace(/<<TAKER_TOKEN_AMOUNT>>/g, data.TAKER_TOKEN_AMOUNT);
  return modifiedMsg;
}