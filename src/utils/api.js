import config from 'config';
import axios from 'axios';

const instance = axios.create({
  baseURL: config.apiBase,
  timeout: 60000
});

const request = (method, url, data) => {
  return new Promise((resolve, reject) => {
    document.body.classList.add('animate');
    instance.request({url, method, data})
    .then(res => resolve(res.data)).catch((err) => {
      if (err.response && err.response.status === 401) {
        window.location = '/';
      }
      reject(err.response);
    })
    .then(() => document.body.classList.remove('animate'))
  });
};

export default {
  get: (endpoint, data) => request('get', endpoint, data),
  post: (endpoint, data) => request('post', endpoint, data),
  put: (endpoint, data) => request('put', endpoint, data),
  del: (endpoint, data) => request('delete', endpoint, data)
};
