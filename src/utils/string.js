export function random() {
  // https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
  return Math.random().toString(36).substring(2) + (new Date()).getTime().toString(36);
}
